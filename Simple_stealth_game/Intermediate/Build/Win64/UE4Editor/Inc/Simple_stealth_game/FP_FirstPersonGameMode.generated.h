// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SIMPLE_STEALTH_GAME_FP_FirstPersonGameMode_generated_h
#error "FP_FirstPersonGameMode.generated.h already included, missing '#pragma once' in FP_FirstPersonGameMode.h"
#endif
#define SIMPLE_STEALTH_GAME_FP_FirstPersonGameMode_generated_h

#define Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonGameMode_h_12_SPARSE_DATA
#define Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonGameMode_h_12_RPC_WRAPPERS
#define Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFP_FirstPersonGameMode(); \
	friend struct Z_Construct_UClass_AFP_FirstPersonGameMode_Statics; \
public: \
	DECLARE_CLASS(AFP_FirstPersonGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Simple_stealth_game"), SIMPLE_STEALTH_GAME_API) \
	DECLARE_SERIALIZER(AFP_FirstPersonGameMode)


#define Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAFP_FirstPersonGameMode(); \
	friend struct Z_Construct_UClass_AFP_FirstPersonGameMode_Statics; \
public: \
	DECLARE_CLASS(AFP_FirstPersonGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Simple_stealth_game"), SIMPLE_STEALTH_GAME_API) \
	DECLARE_SERIALIZER(AFP_FirstPersonGameMode)


#define Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SIMPLE_STEALTH_GAME_API AFP_FirstPersonGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFP_FirstPersonGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SIMPLE_STEALTH_GAME_API, AFP_FirstPersonGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFP_FirstPersonGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SIMPLE_STEALTH_GAME_API AFP_FirstPersonGameMode(AFP_FirstPersonGameMode&&); \
	SIMPLE_STEALTH_GAME_API AFP_FirstPersonGameMode(const AFP_FirstPersonGameMode&); \
public:


#define Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SIMPLE_STEALTH_GAME_API AFP_FirstPersonGameMode(AFP_FirstPersonGameMode&&); \
	SIMPLE_STEALTH_GAME_API AFP_FirstPersonGameMode(const AFP_FirstPersonGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SIMPLE_STEALTH_GAME_API, AFP_FirstPersonGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFP_FirstPersonGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFP_FirstPersonGameMode)


#define Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonGameMode_h_9_PROLOG
#define Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonGameMode_h_12_SPARSE_DATA \
	Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonGameMode_h_12_RPC_WRAPPERS \
	Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonGameMode_h_12_INCLASS \
	Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonGameMode_h_12_SPARSE_DATA \
	Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonGameMode_h_12_INCLASS_NO_PURE_DECLS \
	Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SIMPLE_STEALTH_GAME_API UClass* StaticClass<class AFP_FirstPersonGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
