// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SIMPLE_STEALTH_GAME_FPS_EnemyKOstate_generated_h
#error "FPS_EnemyKOstate.generated.h already included, missing '#pragma once' in FPS_EnemyKOstate.h"
#endif
#define SIMPLE_STEALTH_GAME_FPS_EnemyKOstate_generated_h

#define Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_EnemyKOstate_h_17_SPARSE_DATA
#define Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_EnemyKOstate_h_17_RPC_WRAPPERS
#define Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_EnemyKOstate_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_EnemyKOstate_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFPS_EnemyKOstate(); \
	friend struct Z_Construct_UClass_UFPS_EnemyKOstate_Statics; \
public: \
	DECLARE_CLASS(UFPS_EnemyKOstate, UAnimNotifyState, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Simple_stealth_game"), NO_API) \
	DECLARE_SERIALIZER(UFPS_EnemyKOstate)


#define Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_EnemyKOstate_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUFPS_EnemyKOstate(); \
	friend struct Z_Construct_UClass_UFPS_EnemyKOstate_Statics; \
public: \
	DECLARE_CLASS(UFPS_EnemyKOstate, UAnimNotifyState, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Simple_stealth_game"), NO_API) \
	DECLARE_SERIALIZER(UFPS_EnemyKOstate)


#define Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_EnemyKOstate_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFPS_EnemyKOstate(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFPS_EnemyKOstate) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFPS_EnemyKOstate); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFPS_EnemyKOstate); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFPS_EnemyKOstate(UFPS_EnemyKOstate&&); \
	NO_API UFPS_EnemyKOstate(const UFPS_EnemyKOstate&); \
public:


#define Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_EnemyKOstate_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFPS_EnemyKOstate(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFPS_EnemyKOstate(UFPS_EnemyKOstate&&); \
	NO_API UFPS_EnemyKOstate(const UFPS_EnemyKOstate&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFPS_EnemyKOstate); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFPS_EnemyKOstate); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFPS_EnemyKOstate)


#define Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_EnemyKOstate_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__enemy_owner() { return STRUCT_OFFSET(UFPS_EnemyKOstate, enemy_owner); }


#define Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_EnemyKOstate_h_14_PROLOG
#define Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_EnemyKOstate_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_EnemyKOstate_h_17_PRIVATE_PROPERTY_OFFSET \
	Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_EnemyKOstate_h_17_SPARSE_DATA \
	Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_EnemyKOstate_h_17_RPC_WRAPPERS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_EnemyKOstate_h_17_INCLASS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_EnemyKOstate_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_EnemyKOstate_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_EnemyKOstate_h_17_PRIVATE_PROPERTY_OFFSET \
	Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_EnemyKOstate_h_17_SPARSE_DATA \
	Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_EnemyKOstate_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_EnemyKOstate_h_17_INCLASS_NO_PURE_DECLS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_EnemyKOstate_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SIMPLE_STEALTH_GAME_API UClass* StaticClass<class UFPS_EnemyKOstate>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_EnemyKOstate_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
