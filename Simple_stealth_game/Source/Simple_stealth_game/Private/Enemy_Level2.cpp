// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy_Level2.h"


#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

// Sets default values
AEnemy_Level2::AEnemy_Level2()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AEnemy_Level2::BeginPlay()
{
	Super::BeginPlay();
	BodyMaterial = GetMesh()->CreateDynamicMaterialInstance(0, GetMesh()->GetMaterial(0));
}

// Called every frame
void AEnemy_Level2::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AEnemy_Level2::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AEnemy_Level2::Die()
{
	GetCharacterMovement()->DisableMovement();
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	GetMesh()->GetAnimInstance()->Montage_Play(DeathAnim);
	BodyMaterial->SetVectorParameterValue("BodyColor", FLinearColor::Red);
}

void AEnemy_Level2::UpdateBodyColor(FLinearColor color)
{
	BodyMaterial->SetVectorParameterValue("BodyColor", color);
}
