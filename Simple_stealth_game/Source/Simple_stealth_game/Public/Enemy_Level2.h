// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Enemy_Level2.generated.h"

UCLASS()
class SIMPLE_STEALTH_GAME_API AEnemy_Level2 : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemy_Level2();
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UAnimMontage* DeathAnim;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY()
UMaterialInstanceDynamic* BodyMaterial;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
void Die();
	void UpdateBodyColor(FLinearColor color);
};
